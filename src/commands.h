#ifndef commands_h
#define commands_h
#define NUMBER_OF_COMMANDS 10

//Struct para a definição de um comando.
typedef struct Command{
    char* cmd;
    char* desc;
    void (*effect)(void);
} Command;

extern void GenerateCommands();
extern void CallCommand(char* cmd);
extern void cmd_Ajuda();
extern void cmd_NovoAlbum();

#endif