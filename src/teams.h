#ifndef teams_h
#define teams_h
#define NUMBER_OF_TEAMS 32

//Struct para as bandeiras de seleções:
typedef struct Flag{
    int f;
} Flag;

//Struct para as seleções:
typedef struct Country{
    char* name;
    Flag flag;
    int *team; 
} Country;

extern Country *teamList[NUMBER_OF_TEAMS];
extern void GenerateTeams();

#endif

