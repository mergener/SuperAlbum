#include <stdio.h>
#include <stdlib.h>
#include <conio.h>
#include <string.h>
#include <dirent.h>
#include "teams.h"
#include "album.h"
#include "commands.h"

//Função para obter fragmento de uma string.
char* Substring(char* source, int start, int end){
    if (end < start) return NULL;
    char *new = malloc(sizeof(char)*(end-start)+2);
    int j = 0;
    for (int i = start; i <= end; i++){
        new[j] = source[i];
        j++;
    }
    new[end+1] = (char)"\0";
    return new;
}

//Exibe na tela a lista de todos os álbuns salvos. Retorna true se algum álbum for encontrado.
int PrintAlbums(char albumsDirectory[]){
    struct dirent *de; 
    DIR *dr = opendir(albumsDirectory); 
    if (dr == NULL) return 0;
    int i = 0;
    while ((de = readdir(dr)) != NULL){
        char *s = Substring(de->d_name,strlen(de->d_name)-6,strlen(de->d_name)-1);
        char *out = Substring(de->d_name,0,strlen(de->d_name)-7);   
        if (strcmp(s,".album")==0){
            puts(out);
            i=1;
        }
        free(s);
        free(out);
    }
    closedir(dr);
    if (i == 0){
        printf("Nenhum album foi encontrado.\n");
        return 0;
    }
    return 1;
}

//Salva um álbum na pasta Meus Albuns.
void SaveAlbum(Album* which){
    char s[15];
    char dir[] = "Meus Albuns/";
    printf("Escreva o nome com o qual deseja salvar seu álbum: ");
    gets(s);
    strcat(dir,s);
    strcat(dir,".album");
    fopen(dir,"w");

    //Salva todas as figurinhas:

    printf("Album salvo com sucesso!");
}

//Bloco principal do programa.
int main(int argc, char **argv){
    printf("Bem vindo ao SuperAlbum! O que deseja fazer? (Digite 'ajuda' para obter uma lista de comandos)\n");
    GenerateTeams();
    GenerateCommands();
    char input[10];
    for (;;){
        scanf("%c",&input);
        getchar();
        CallCommand(input);
    }
    return 0;
}
